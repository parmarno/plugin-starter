<?php
/* 	Plugin Name:  Starter Plugin
*	Plugin URI:   https://i-makeweb.com/
*	Description:  Starter Plugin
*	Version:      1
*	Author:       Ekkarach Kulwong
*	Author URI:   https://i-makeweb.com/
*	License:      GPL2
*	License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*	Text Domain:  imk
*	Domain Path:  /languages
*	*/

	if( ! function_exists('add_script_admin') ) :
		function add_script_admin($hook){
			wp_enqueue_script( 'admin-js', plugins_url('assets/js/app.js', __FILE__) );
			wp_localize_script( 'admin-js', 'get_ajax',[
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'security' => wp_create_nonce( 'admin-js-filter' )
			]);
		}	
	endif;

	add_action( 'admin_enqueue_scripts', 'add_script_admin' );


	if( !function_exists('get_ajax') ) :
		function get_ajax(){
			$security = $_POST['security'];
			if (!wp_verify_nonce( $security, 'admin-js-filter')):
				die ( 'Busted!');
			endif;

			echo "lorem ipsum";
		}
	endif;
	add_action( 'wp_ajax_get_ajax', 'get_ajax');
	add_action( 'wp_ajax_nopriv_get_ajax', 'get_ajax' );
