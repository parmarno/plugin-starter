(function ($) {
	
	"use strict";
	
	$(document).ready(function() {

		// call .btn button event
		$('body').on('click', '.btn', function(){

			$.post(get_ajax.ajax_url, {
					
				action : 'get_ajax',
				security : get_ajax.security,
				q : Math.random()

			}, function(response){
				
	           console.log(response);

	        });
		});	
	});

}(jQuery));

